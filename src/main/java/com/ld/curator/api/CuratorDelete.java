package com.ld.curator.api;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author:ld
 * @create:2020-04-30 13:44
 * @description:
 */
public class CuratorDelete {

    //客户端对象
    private CuratorFramework client;
    //连接地址
    private final String IP = "127.0.0.1:2181";

    /**
     * 获取连接
     */
    @Before
    public void before(){
        //重试机制，即每隔1秒，重试一次，一共可以重试3次
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000,3);
        //方式一：获取客户端对象
//        client = CuratorFrameworkFactory.newClient(IP, 5000, 3000, retryPolicy);
        //方式二：获取客户端对象
        client = CuratorFrameworkFactory
                .builder()
                .connectString(IP)//连接地址
                .connectionTimeoutMs(3000) //连接超时时间
                .sessionTimeoutMs(5000) //会话超时时间
                .retryPolicy(retryPolicy) //重试机制
                .namespace("base") //命名空间，即指定根节点，后续操作都在根节点下。注意：开头不要加 “/”
                .build();


        //开启连接
        client.start();
    }

    /**
     * 关闭连接
     */
    @After
    public void close(){
        client.close();
    }


    /**
     * 删除节点
     */
    @Test
    public void delete1() throws Exception {
        client.delete()
                .deletingChildrenIfNeeded() //用于递归删除
                .withVersion(-1) //指定版本号
                .forPath("/node");//指定节点路径
    }
}
