package com.ld.curator.api;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.BackgroundCallback;
import org.apache.curator.framework.api.CuratorEvent;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author:ld
 * @create:2020-04-23 10:10
 * @description: 使用Curator框架创建节点
 */
public class CuratorCreate {

    //客户端对象
    private CuratorFramework client;
    //连接地址
    private final String IP = "127.0.0.1:2181";

    /**
     * 获取连接
     */
    @Before
    public void before(){
        //重试机制，即每隔1秒，重试一次，一共可以重试3次
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000,3);
        //方式一：获取客户端对象
//        client = CuratorFrameworkFactory.newClient(IP, 5000, 3000, retryPolicy);
        //方式二：获取客户端对象
        client = CuratorFrameworkFactory
                .builder()
                .connectString(IP)//连接地址
                .connectionTimeoutMs(3000) //连接超时时间
                .sessionTimeoutMs(5000) //会话超时时间
                .retryPolicy(retryPolicy) //重试机制
                .namespace("base") //命名空间，即指定根节点，后续操作都在根节点下。注意：开头不要加 “/”
                .build();


        //开启连接
        client.start();
    }

    /**
     * 关闭连接
     */
    @After
    public void close(){
        client.close();
    }

    /**
     * 同步创建节点
     */
    @Test
    public void create1() throws Exception {
        String path = client.create() //创建节点
                .creatingParentContainersIfNeeded() //如果需要创建父节点，即递归创建
                .withMode(CreateMode.PERSISTENT) //指定节点类型
                .withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE) //指定ACL
                .forPath("/create/node1", "node1".getBytes());//指定路径和节点数据
        System.out.println("节点路径：" + path);
    }

    /**
     * 异步创建节点
     */
    @Test
    public void create2() throws Exception {
        client.create() //创建节点
                .creatingParentContainersIfNeeded() //如果需要创建父节点，即递归创建
                .withMode(CreateMode.PERSISTENT) //指定节点类型
                .withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE) //指定ACL
                //异步创建节点
                //arg1: 回调接口
                .inBackground(new BackgroundCallback() {

                    //arg1: 连接对象
                    //arg2: 事件对象
                    @Override
                    public void processResult(CuratorFramework curatorFramework, CuratorEvent curatorEvent) throws Exception {
                        //结果
                        System.out.println("执行结果：" + curatorEvent.getResultCode());
                        //事件类型
                        System.out.println("事件类型：" + curatorEvent.getType());
                        //节点名称
                        System.out.println("节点名称：" + curatorEvent.getName());
                        //节点路径
                        System.out.println("节点路径：" + curatorEvent.getPath());
                    }
                })
                .forPath("/create/node2", "node2".getBytes());
        Thread.sleep(5000);
        System.out.println("结束");
    }
}
