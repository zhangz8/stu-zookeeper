package com.ld.curator.api;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.BackgroundCallback;
import org.apache.curator.framework.api.CuratorEvent;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.data.Stat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author:ld
 * @create:2020-04-23 11:41
 * @description: Curator框架 设置节点数据
 */
public class CuratorSet {

    //客户端对象
    private CuratorFramework client;
    //连接地址
    private final String IP = "127.0.0.1:2181";

    /**
     * 获取连接
     */
    @Before
    public void before(){
        //重试机制，即每隔1秒，重试一次，一共可以重试3次
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000,3);
        //方式一：获取客户端对象
//        client = CuratorFrameworkFactory.newClient(IP, 5000, 3000, retryPolicy);
        //方式二：获取客户端对象
        client = CuratorFrameworkFactory
                .builder()
                .connectString(IP)//连接地址
                .connectionTimeoutMs(3000) //连接超时时间
                .sessionTimeoutMs(5000) //会话超时时间
                .retryPolicy(retryPolicy) //重试机制
                .namespace("base") //命名空间，即指定根节点，后续操作都在根节点下。注意：开头不要加 “/”
                .build();


        //开启连接
        client.start();
    }

    /**
     * 关闭连接
     */
    @After
    public void close(){
        client.close();
    }

    /**
     * 同步设置节点数据
     */
    @Test
    public void set1() throws Exception {
        Stat stat = client.setData() //设置节点数据
                .withVersion(-1) //版本
                .forPath("/set/node1", "node1".getBytes());//设置数据
        System.out.println("节点版本号：" + stat.getVersion());
    }

    /**
     * 异步修改节点数据
     */
    @Test
    public void set2() throws Exception {
        Stat stat = client.setData() //设置节点数据
                .withVersion(-1) //版本
                .inBackground(new BackgroundCallback() {
                    @Override
                    public void processResult(CuratorFramework curatorFramework, CuratorEvent curatorEvent) throws Exception {
                        //结果
                        System.out.println("执行结果：" + curatorEvent.getResultCode());
                        //事件类型
                        System.out.println("事件类型：" + curatorEvent.getType());
                        //节点名称
                        System.out.println("节点名称：" + curatorEvent.getName());
                        //节点路径
                        System.out.println("节点路径：" + curatorEvent.getPath());
                        //节点数据
                        System.out.println("节点数据" + new String(curatorEvent.getData()));
                    }
                })
                .forPath("/set/node1", "node1111".getBytes());//设置数据
        Thread.sleep(3000);
        System.out.println(stat.getVersion());
    }

    /**
     * 为节点设置ACL
     */
    @Test
    public void setAcl() throws Exception {
        Stat stat = client.setACL()
                .withVersion(-1)
                .withACL(ZooDefs.Ids.READ_ACL_UNSAFE)
                .forPath("/node");
        System.out.println("版本号：" + stat.getVersion());
    }
}
