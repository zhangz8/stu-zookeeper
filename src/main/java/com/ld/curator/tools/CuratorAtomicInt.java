package com.ld.curator.tools;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.atomic.AtomicValue;
import org.apache.curator.framework.recipes.atomic.DistributedAtomicInteger;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.retry.RetryNTimes;

/**
 * @author:ld
 * @create:2020-04-30 16:01
 * @description: 分布式计数器
 */
public class CuratorAtomicInt {

    private static String atomicint_path = "/curator_atomicint_path";

    private static CuratorFramework client = CuratorFrameworkFactory
            .builder()
            .connectString("127.0.0.1:2181")
            .retryPolicy(new ExponentialBackoffRetry(1000,3))
            .build();

    public static void main(String[] args) throws Exception {
        //开启客户端连接
        client.start();

        //创建分布式原子整形对象
        DistributedAtomicInteger atomicInteger = new DistributedAtomicInteger(client, atomicint_path, new RetryNTimes(3, 100));
        AtomicValue<Integer> rc = atomicInteger.add(4);
        System.out.println("result：" + rc.postValue());
    }
}
