package com.ld.zookeeper.tools;

/**
 * @author:ld
 * @create:2020-04-22 15:56
 * @description: zookeeper 原生API 实现分布式锁
 * 思路：
 *  1、先在zookeeper中创建一个永久节点lock
 *  2、需要获取锁的时候，在lock节点下创建一个临时顺序子节点，节点前缀可以自定义
 *  3、获取lock节点下所有的子节点，并进行排序，判断自己是否为第一个
 *  4、是第一个就表明获取到了锁，执行业务代码
 *  5、不是第一个就对自己的前一个节点进行监听，当前一个节点被删除时，重新从第三步执行
 *  6、当执行完业务代码后，释放锁，即删除当前节点
 *
 */
public class ZookeeperLock {
}
